-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.6.42 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  9.5.0.5328
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 导出 rmp 的数据库结构
CREATE DATABASE IF NOT EXISTS `rmp` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `rmp`;

-- 导出  表 rmp.rmp_api_data_history 结构
CREATE TABLE IF NOT EXISTS `rmp_api_data_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apiName` text COLLATE utf8mb4_unicode_ci,
  `apiIdentity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apiData` text COLLATE utf8mb4_unicode_ci,
  `saveTime` datetime DEFAULT NULL,
  `mark` text COLLATE utf8mb4_unicode_ci,
  `testTime` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filePath` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- 正在导出表  rmp.rmp_api_data_history 的数据：~2 rows (大约)
DELETE FROM `rmp_api_data_history`;
/*!40000 ALTER TABLE `rmp_api_data_history` DISABLE KEYS */;
INSERT INTO `rmp_api_data_history` (`id`, `apiName`, `apiIdentity`, `apiData`, `saveTime`, `mark`, `testTime`, `filePath`) VALUES
	(20, '3123124', '32131231', '{"applicationResources":[{"tags":"wangyj","host":"39.108.62.206","logSize":"","freeMem":"54.26%","ioRead":"0.00MB/S","logCycle":"","cpuRequirements":"","tranQueue":"","serverType":"linux","memoryRequirements":"","networkUtilization":"","freeCpu":"88.00%","logNum":"","ioWrite":"0.00MB/S","backupResource":"","sharedMemory":"","backupNetworkUtilization":"","containerResource":""},{"tags":"wangyj","host":"39.108.62.206","logSize":"","freeMem":"54.26%","ioRead":"0.00MB/S","logCycle":"","cpuRequirements":"","tranQueue":"","serverType":"linux","memoryRequirements":"","networkUtilization":"","freeCpu":"88.00%","logNum":"","ioWrite":"0.00MB/S","backupResource":"","sharedMemory":"","backupNetworkUtilization":"","containerResource":""}],"databaseResources":[{"tags":"wangyj","host":"39.108.62.206","freeCpu":"84.14%","connect":"","freeMem":"52.29%","ioWait":"0.00%","serverType":"linux"}],"clusterResources":[{"tags":"wangyj","Inclination":"","host":"39.108.62.206","freeCpu":"82.00%","storageInc":"","freeMem":"50.80%","serverType":"linux"}]}', '2018-06-05 17:24:24', '1241241', NULL, 'exportExcel\\3123124.xlsx'),
	(21, '42141', '312412', '{"applicationResources":[{"tags":"wangyj","host":"39.108.62.206","logSize":"","freeMem":"52.29%","ioRead":"0.00MB/S","logCycle":"","cpuRequirements":"","tranQueue":"","serverType":"linux","memoryRequirements":"","networkUtilization":"","freeCpu":"84.14%","logNum":"","ioWrite":"0.00MB/S","backupResource":"","sharedMemory":"","backupNetworkUtilization":"","containerResource":""}],"databaseResources":[{"tags":"wangyj","host":"39.108.62.206","freeCpu":"88.00%","connect":"","freeMem":"54.26%","ioWait":"0.00%","serverType":"linux"}],"clusterResources":[{"tags":"wangyj","Inclination":"","host":"39.108.62.206","freeCpu":"89.91%","storageInc":"","freeMem":"55.53%","serverType":"linux"}]}', '2018-06-05 17:36:42', '1412', NULL, 'exportExcel\\42141.xlsx');
/*!40000 ALTER TABLE `rmp_api_data_history` ENABLE KEYS */;

-- 导出  表 rmp.rmp_file_info 结构
CREATE TABLE IF NOT EXISTS `rmp_file_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filePath` text COLLATE utf8mb4_unicode_ci,
  `configId` int(11) DEFAULT NULL,
  `fileSize` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `serverCount` int(5) DEFAULT NULL,
  `infoCount` int(11) DEFAULT NULL,
  `serverInfos` text COLLATE utf8mb4_unicode_ci,
  `mark` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `rmp_file_info_fk_config_id` (`configId`),
  CONSTRAINT `rmp_file_info_fk_config_id` FOREIGN KEY (`configId`) REFERENCES `rmp_user_config` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- 正在导出表  rmp.rmp_file_info 的数据：~1 rows (大约)
DELETE FROM `rmp_file_info`;
/*!40000 ALTER TABLE `rmp_file_info` DISABLE KEYS */;
INSERT INTO `rmp_file_info` (`id`, `fileName`, `filePath`, `configId`, `fileSize`, `createTime`, `serverCount`, `infoCount`, `serverInfos`, `mark`) VALUES
	(9, '1549623091587_admin123.json', 'files\\1549623091587_admin123.json', 17, '2.01KB', '2019-02-08 18:51:32', 1, 13, '[{"memInfo":2054140,"lastTime":"2019-02-08 18:51:23","connectStatus":"true","uname":"Linux","count":13,"cpuInfo":1,"password":"DCtest@0123","viewId":0,"port":22,"serverType":"linux","host":"47.110.45.178","mountDevices":["vda"],"startTime":"2019-02-08 18:50:09","id":15,"info":{"tcpInfo":{"CLOSE_WAIT":"102","ESTABLISHED":"8","LISTEN":"8","TIME_WAIT":"0"},"networkInfo":{"tx":"0","rx":"0"},"ioWait":"0","freeMem":"59","time":"2019-02-08 18:51:28","diskInfo":{"rootDisk":"23","userDisk":"0"},"freeCpu":"98","ioInfo":{"ioRead":"0.00","ioWrite":"0.00"}},"username":"root"}]', '这是备注');
/*!40000 ALTER TABLE `rmp_file_info` ENABLE KEYS */;

-- 导出  表 rmp.rmp_real_time_info 结构
CREATE TABLE IF NOT EXISTS `rmp_real_time_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serverId` int(11) DEFAULT NULL,
  `configId` int(11) DEFAULT NULL,
  `infoJson` text COLLATE utf8mb4_unicode_ci,
  `saveTime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `rmp_real_time_info_fk_server_id` (`serverId`),
  KEY `rmp_real_time_info_fk_config_id` (`configId`),
  CONSTRAINT `rmp_real_time_info_fk_config_id` FOREIGN KEY (`configId`) REFERENCES `rmp_user_config` (`id`) ON DELETE CASCADE,
  CONSTRAINT `rmp_real_time_info_fk_server_id` FOREIGN KEY (`serverId`) REFERENCES `rmp_server_info` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- 正在导出表  rmp.rmp_real_time_info 的数据：~0 rows (大约)
DELETE FROM `rmp_real_time_info`;
/*!40000 ALTER TABLE `rmp_real_time_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `rmp_real_time_info` ENABLE KEYS */;

-- 导出  表 rmp.rmp_server_info 结构
CREATE TABLE IF NOT EXISTS `rmp_server_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `configId` int(11) NOT NULL,
  `realHost` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `port` int(6) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serverType` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `mark` text COLLATE utf8mb4_unicode_ci,
  `lastUseTime` datetime DEFAULT NULL,
  `tags` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_rmp_server_info_rmp_user_config` (`configId`),
  CONSTRAINT `FK_rmp_server_info_rmp_user_config` FOREIGN KEY (`configId`) REFERENCES `rmp_user_config` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- 正在导出表  rmp.rmp_server_info 的数据：~2 rows (大约)
DELETE FROM `rmp_server_info`;
/*!40000 ALTER TABLE `rmp_server_info` DISABLE KEYS */;
INSERT INTO `rmp_server_info` (`id`, `host`, `configId`, `realHost`, `port`, `username`, `password`, `serverType`, `parameters`, `createTime`, `mark`, `lastUseTime`, `tags`) VALUES
	(13, '123.3123.312', 20, '22', 22, '22', '22', 'linux', NULL, '2019-02-08 15:16:10', NULL, NULL, NULL),
	(14, '123.3123.31', 20, '22', 22, '22', '22', 'linux', NULL, '2019-02-08 17:00:38', NULL, NULL, NULL);
/*!40000 ALTER TABLE `rmp_server_info` ENABLE KEYS */;

-- 导出  表 rmp.rmp_user_config 结构
CREATE TABLE IF NOT EXISTS `rmp_user_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userKey` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `configInfoJson` text COLLATE utf8mb4_unicode_ci,
  `customLinuxCommand` text COLLATE utf8mb4_unicode_ci,
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


/*!40000 ALTER TABLE `rmp_user_config` ENABLE KEYS */;

CREATE TABLE `rmp_command_config` (
                                    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
                                    `configId` int(11) NOT NULL COMMENT 'userkey',
                                    `briefCommand` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '快捷指令',
                                    `command` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '指令',
                                    `useFlag` int(1) NOT NULL DEFAULT '1' COMMENT '是否可用： 1可用；0不可用',
                                    `mark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT '备注',
                                    `createTime` datetime DEFAULT NULL COMMENT '创建时间',
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='指令配置表';


/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
