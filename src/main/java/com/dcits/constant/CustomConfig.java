package com.dcits.constant;

import com.dcits.tool.RmpUtil;
import com.dcits.tool.StringUtils;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;

import java.io.File;

/**
 * @author xuwangcheng14@163.com
 * @version 1.0.0
 * @description
 * @date 2022/6/1  20:41
 */
public class CustomConfig {

    public static String DATA_FILE_SAVE_PATH = PathKit.getWebRootPath() + File.separator + RmpUtil.DATA_FILE_SAVE_PATH;
    public static boolean SHARE_SERVER_LIST = false;

    static {
        Prop p = PropKit.use("init.properties");
        String dataSavePath = p.get("config.dataSavePath");
        if (StringUtils.isNotEmpty(dataSavePath)) {
            DATA_FILE_SAVE_PATH = dataSavePath;
        }
        File f = new File(DATA_FILE_SAVE_PATH);
        if (!f.exists()) {
            f.mkdirs();
        }

        SHARE_SERVER_LIST = p.getBoolean("config.shareServerList");
    }
}
