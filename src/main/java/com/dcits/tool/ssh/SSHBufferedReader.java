package com.dcits.tool.ssh;

import ch.ethz.ssh2.Session;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

public class SSHBufferedReader extends BufferedReader {
	private Session session;

	public SSHBufferedReader(Reader in, Session session) {
		super(in);
	}
	
	@Override
	public void close() throws IOException {
		try {
			super.close();
		} catch (IOException e) {
			throw e;
		} finally {
			if (session != null) {
				session.close();
			}
		}
		
	}

}
