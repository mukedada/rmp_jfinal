package com.dcits.dto;

import com.alibaba.fastjson.JSON;

import javax.websocket.DecodeException;
import javax.websocket.EndpointConfig;
import java.io.Serializable;
import java.util.List;

/**
 * description: todo
 *
 * @author sun jun
 * @className ResponseSocketDTO
 * @date 2020/9/21 15:31
 */
public class ResponseSockeDecoder implements javax.websocket.Decoder.Text<ResponseSocketDTO> {


    @Override
    public ResponseSocketDTO decode(String responseSocketDTO) throws DecodeException {
        return JSON.parseObject(responseSocketDTO, ResponseSocketDTO.class);
    }

    @Override
    public boolean willDecode(String s) {
        return false;
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }
}
