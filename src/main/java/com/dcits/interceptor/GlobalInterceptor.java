package com.dcits.interceptor;

import com.dcits.tool.SessionKit;
import org.apache.log4j.Logger;

import com.dcits.constant.ConstantReturnCode;
import com.dcits.dto.RenderJSONBean;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

public class GlobalInterceptor implements Interceptor {
	private static final Logger logger = Logger.getLogger(GlobalInterceptor.class);

	@Override
	public void intercept(Invocation inv) {
        SessionKit.put(inv.getController().getSession(true));
		try {
			inv.invoke();
		} catch (Exception e) {
			logger.error(inv.getActionKey() + " 执行出错.", e);
			RenderJSONBean json = new RenderJSONBean();
			json.setReturnCode(ConstantReturnCode.SYSTEM_ERROR);
			json.setMsg(e.getMessage() == null ? "未知错误" : e.getMessage());
			
			inv.getController().renderJson(json);
		} finally {
            SessionKit.remove();
        }
	}

}
