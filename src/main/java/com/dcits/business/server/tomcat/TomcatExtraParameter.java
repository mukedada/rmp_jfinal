package com.dcits.business.server.tomcat;

public class TomcatExtraParameter {

	/**
	 * web访问端口,默认8080
	 */
	private Integer webPort;
	/**
	 * 监控项目路径,默认/,即根目录
	 */
	private String projectName;
	private String javaHome;
	private String linuxLoginUsername;
	private String linuxLoginPassword;
	private String startScriptPath;
	
	public TomcatExtraParameter() {
		super();

	}
	public TomcatExtraParameter(Integer webPort, String projectName, String javaHome, String linuxLoginUsername,
			String linuxLoginPassword, String startScriptPath) {
		super();
		this.webPort = webPort;
		this.projectName = projectName;
		this.javaHome = javaHome;
		this.linuxLoginUsername = linuxLoginUsername;
		this.linuxLoginPassword = linuxLoginPassword;
		this.startScriptPath = startScriptPath;
	}
	public Integer getWebPort() {
		return webPort;
	}
	public void setWebPort(Integer webPort) {
		this.webPort = webPort;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getJavaHome() {
		return javaHome;
	}
	public void setJavaHome(String javaHome) {
		this.javaHome = javaHome;
	}
	public String getLinuxLoginUsername() {
		return linuxLoginUsername;
	}
	public void setLinuxLoginUsername(String linuxLoginUsername) {
		this.linuxLoginUsername = linuxLoginUsername;
	}
	public String getLinuxLoginPassword() {
		return linuxLoginPassword;
	}
	public void setLinuxLoginPassword(String linuxLoginPassword) {
		this.linuxLoginPassword = linuxLoginPassword;
	}
	public String getStartScriptPath() {
		return startScriptPath;
	}
	public void setStartScriptPath(String startScriptPath) {
		this.startScriptPath = startScriptPath;
	}
	
}
