package com.dcits.business.server.jvm;

import org.apache.log4j.Logger;

import com.dcits.business.server.BaseServerController;
import com.dcits.business.server.ViewServerInfo;
import com.dcits.business.server.dubbo.DubboServer;
import com.dcits.business.server.linux.LinuxServer;
import com.dcits.business.server.tomcat.TomcatServer;
import com.dcits.business.server.weblogic.WeblogicServer;
import com.dcits.business.userconfig.UserSpace;
import com.dcits.constant.ConstantReturnCode;
import com.dcits.tool.ssh.SSHUtil;


public class JvmServerController extends BaseServerController {
	
	private static final Logger logger = Logger.getLogger(JvmServerController.class);

	/**
	 * jvm的重连不仅仅需要重连linux服务,也需要检查pid的有效性
	 */
	@Override
	public void reconnect() {

		UserSpace space = UserSpace.getUserSpace(getPara("userKey"));
		JvmServer info = (JvmServer) space.getServerInfo(getPara("serverType"), getParaToInt("viewId"));
		info.disconect();
		String flag = info.connect();
		
		if (!"true".equalsIgnoreCase(flag)) {
			renderError(ConstantReturnCode.SERVER_CONNECT_FAILED, flag);
			return;
		}
		
		if (info.getRelevanced()) {
			try {
				if (info.getWeblogicPort() != null) info.setProcessPid(info.getWeblogicPort());
				if (info.getTomcatPort() != null) info.setProcessPid(info.getTomcatPort());
				if (info.getDubboPort() != null) info.setProcessPid(info.getDubboPort());
				info.execJstat();
				info.getMonitoringInfo();
			} catch (Exception e) {

				renderError(ConstantReturnCode.SERVER_CONNECT_FAILED, e.getMessage());
				return;
			}			
		}				
		renderSuccess(null, "重连成功!");
	}	
	
	public void stack() {
		UserSpace space = UserSpace.getUserSpace(getPara("userKey"));
		JvmServer jvm = (JvmServer) space.getServerInfo(JvmServer.SERVER_TYPE_NAME, getParaToInt("viewId"));
		
		//执行jstack命令
		try {
			String str = SSHUtil.execCommand(jvm.getJschSession(), jvm.getJavaHome() + "/bin/jstack " + jvm.getPid(), 9999999, 2, "");
			renderSuccess(str, "获取成功!");
		} catch (Exception e) {

			logger.error("执行jstack命令失败!", e);
			renderError(ConstantReturnCode.SERVER_CONNECT_FAILED, "执行jstack命令失败：[" + jvm.getJavaHome() + "/jstack " + jvm.getPid() + "]");
		}
		
	}
	
	public void addJvm() {
		UserSpace space = UserSpace.getUserSpace(getPara("userKey"));
		String serverType = getPara("serverType");
		int viewId = getParaToInt("viewId");
		
		ViewServerInfo serverInfo = space.getServerInfo(serverType, viewId);
		
		if (serverInfo == null) {
			renderError(ConstantReturnCode.VALIDATE_FAIL, "该服务器不存在!");
			return;
		}
		
		JvmServer jvm = null;
		
		try {
			if (LinuxServer.SERVER_TYPE_NAME.equals(serverType)) {
				String processName = getPara("processName");
				int pid = getParaToInt("pid");
				
				jvm = JvmServer.addJvm((LinuxServer) serverInfo, pid, processName);
			}
			
			if (TomcatServer.SERVER_TYPE_NAME.equals(serverType) ) {
				jvm = JvmServer.addJvm((TomcatServer) serverInfo);
			}
			
			if (WeblogicServer.SERVER_TYPE_NAME.equals(serverType)) {
				jvm = JvmServer.addJvm((WeblogicServer) serverInfo);
			}
			
			if (DubboServer.SERVER_TYPE_NAME.equals(serverType)) {
				jvm = JvmServer.addJvm((DubboServer) serverInfo);
			}
		} catch (Exception e) {

			logger.error("添加jvm监控信息时发生了错误！", e);
			renderError(ConstantReturnCode.SYSTEM_ERROR, "添加jvm监控信息时发生了错误 " + e.getMessage() == null ? "" : e.getMessage());
			return;
		}
		
		space.getServers().get(JvmServer.SERVER_TYPE_NAME).add(jvm);
		
		renderSuccess(null, "添加jvm监控成功!");
	}
	
}
