package com.dcits.business.server.redis;

import org.apache.log4j.Logger;

import com.dcits.business.server.BaseServerController;

public class RedisServerController extends BaseServerController {
	
	private static final Logger logger = Logger.getLogger(RedisServerController.class);
	
	//连接到该数据库执行命令
	public void execCommand() {
		renderSuccess(null, "该功能尚未完成!");
	}
	
	public void breakCommand (){
		renderSuccess(null, "成功发送停止命令!");
	}
	
	
	//获取redis命令执行信息
	public void handleInfo() {
		renderSuccess(null, "该功能尚未完成!");
	}
}
