package com.dcits.business.server.websocker;

import com.alibaba.fastjson.JSONObject;
import com.dcits.business.server.linux.LinuxServer;
import com.dcits.business.userconfig.UserSpace;
import com.dcits.dto.ResponseSockeDecoder;
import com.dcits.dto.ResponseSockeEecoder;
import com.dcits.dto.ResponseSocketDTO;
import com.dcits.mvc.common.service.CommandConfigService;
import com.dcits.mvc.common.service.ServerInfoService;
import com.dcits.tool.StringUtils;
import com.dcits.tool.ssh.SSHUtil;
import org.apache.log4j.Logger;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;

/**
 * description: todo
 *
 * @author sun jun
 * @className WebSocketController
 * @date 2020/8/25 8:47
 */
@ServerEndpoint(value = "/websocket", decoders = {ResponseSockeDecoder.class}, encoders = {ResponseSockeEecoder.class})
public class WebSocketController {

    private ServerInfoService serverInfoService = new ServerInfoService();
    private CommandConfigService commandService = new CommandConfigService();

    public static List<String> commonTags = new ArrayList<>();
    public static Boolean FLAG = true;

    private Session session;
    private String userKey;
    private String ids;
    private String tag;
    private String tags;
    private String command;

    /**
     * concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。
     * 在外部可以获取此连接的所有websocket对象，并能对其触发消息发送功能，我们的定时发送核心功能的实现在与此变量
     */
    private static CopyOnWriteArraySet<WebSocketController> webSocketSet = new CopyOnWriteArraySet<WebSocketController>();
    // 执行线程池
//    private final static ExecutorService cachedThreadPool = Executors.newFixedThreadPool(10);
    private final static ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
    // 日志
    public static final Logger logger = Logger.getLogger(WebSocketController.class);
    private static ScheduledExecutorService executorSend;  //发送消息线程池
    //待发送的消息队列
    public static ArrayDeque<WebSocketMessageQueue> sendQueue = new ArrayDeque<WebSocketMessageQueue>();


    public WebSocketController() {
        System.out.println(" WebSocket init~~~");
        WebSocketController.executorSend = new ScheduledThreadPoolExecutor(1);
    }

    @OnOpen
    public void onOpen(Session session) throws IOException {
        //加入set中
        webSocketSet.add(this);

        this.session = session;
        System.out.println(" WebSocket onOpen~~~");
    }

    @OnClose
    public void onClose(Session session) {
        webSocketSet.remove(this);
        // socket断开后，中止当前socket所有线程正在执行的任务
        for (String commonTag : commonTags) {
            SSHUtil.stopExecCommand(commonTag);
        }
        System.out.println("  WebSocket  onClose");
    }

    @OnError
    public void onError(Session session, Throwable error) {
        System.out.println("WebSocket >> 发生错误");
        error.printStackTrace();
    }

    @OnMessage
    public void onMessage(String responseStr, Session session) throws IOException, EncodeException {
        JSONObject jsStr = JSONObject.parseObject(responseStr);
        // 0：正常消息； 1：心跳检测
        if ("0".equals((jsStr.getString("type")))){
            this.setIds(jsStr.getString("ids"));
            this.setTag(jsStr.getString("tag"));
            this.setTags(jsStr.getString("tags"));
            this.setUserKey(jsStr.getString("userKey"));
            this.setCommand(jsStr.getString("command"));
            commonTags.add(this.tag);
            execCommand(this.getCommand());
        }else {
           sendMessage("pong");
        }

    }

    private void execCommand(String command) {
        int configId = UserSpace.getUserSpace(this.getUserKey()).getUserConfig().getId();
        try {
            commonExec(command);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (EncodeException e) {
            e.printStackTrace();
        }
    }

    private void commonExec(String briefCommand) throws IOException, EncodeException {
        UserSpace space = UserSpace.getUserSpace(this.getUserKey());

        if (StringUtils.isNotEmpty(this.getTags())) {
            for (String tag : getTags().split(",")){
                SSHUtil.stopExecCommand(tag);
            }
            return;
        }

        for (String id : this.getIds().split(",")) {

            final LinuxServer server = (LinuxServer) space.getServerInfo(this.getUserKey(), Integer.valueOf(id));
            final Session execSession  = this.session;
            final String execTag  = this.tag;

            if (server == null) {
                continue;
            }

            final JSONObject obj = new JSONObject();
            ResponseSocketDTO outDTO = new ResponseSocketDTO();
            if (server.getChannel() != null){
                SSHUtil.transToSSH(server.getChannel(),briefCommand);
                continue;
            }
            cachedThreadPool.execute (new Runnable() {
                @Override
                public void run() {

                    outDTO.setHost(server.getHost());
                    outDTO.setViewId(server.getViewId());
                    try {
                      SSHUtil.channelExec(server, briefCommand,session,outDTO,execTag);
                    } catch (Exception e) {
                        logger.error(e.getMessage());
                        outDTO.setCommandResponse("Sorry, this connection is closed.");
                        try {
                            execSession.getBasicRemote().sendObject(outDTO);
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        } catch (EncodeException e1) {
                            e1.printStackTrace();
                        }
                        logger.error("Sorry, this connection is closed:" +e);
                    }
                }

            });

        }
    }


    private void sendMessage(String message) throws IOException {
        this.session.getBasicRemote().sendText(message);
    }

    public void sendObject(Object message) throws IOException, EncodeException {
        this.session.getBasicRemote().sendObject(message);
    }

    /**
     * description:  判断服务器是否连接
     *
     * @author sunjun
     * @date 2020/9/22 10:05
     */
    public void sendServerConnect() throws IOException, EncodeException {
        UserSpace space = UserSpace.getUserSpace(this.getUserKey());

        for (String id : this.getIds().split(",")) {
            ResponseSocketDTO outDTO = new ResponseSocketDTO();
            final LinuxServer server = (LinuxServer) space.getServerInfo(this.getUserKey(), Integer.valueOf(id));
            if (server == null) {
                continue;
            }
            try {
                // 根据是否能打开连接判断connect是否存在
                if (!server.getJschSession().isConnected()) {
                    throw new Exception("未连接到服务器");
                }
            } catch (Exception e) {
                outDTO.setHost(server.getHost());
                outDTO.setViewId(server.getViewId());
                outDTO.setCommandResponse("Sorry, this connection is closed");
                logger.error(e.getMessage());
                this.session.getBasicRemote().sendObject(outDTO);
            }
        }

    }

    /**
     * description: 向所有客户端发送消息
     *
     * @param message
     * @return void
     * @author sunjun
     * @date 2020/9/18 9:54
     */
    public static void sendConnectInfo() throws IOException, EncodeException {
        for (WebSocketController productWebSocket : webSocketSet) {
            if (StringUtils.isNotEmpty(productWebSocket.getIds())) {
                productWebSocket.sendServerConnect();
            }
        }
    }

    public static void PutMessageToQueue(Session session, ResponseSocketDTO message, String tag){
        WebSocketMessageQueue queue = new WebSocketMessageQueue();
        queue.setSession(session);
        queue.setResponseSocketDTO(message);
        queue.setTag(tag);
        //将消息放入 待发送的消息 队列
        WebSocketController.sendQueue.add(queue);
    }

    public static void sendQueueMsg(){
        executorSend.execute(sendRunnable);
    }


    private static final Runnable sendRunnable = new Runnable() {
        @Override
        public void run() {
            int index = 0;
            while (true) {
                WebSocketMessageQueue webSocketMsg = sendQueue.poll();   //从队列中取消息
                if (webSocketMsg != null) {
                    try {
                        webSocketMsg.getSession().getBasicRemote().sendObject(webSocketMsg.getResponseSocketDTO());
                    } catch (Exception e) {
                       logger.error(e.getMessage());
                       break;
                    }
                } else {  //如果取不到消息时就跳出while (true)循环，避免无必要的消耗cpu
                    break;
                }
            }
            WebSocketController.FLAG = true;
        }
    };

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public static void setWebSocketSet(CopyOnWriteArraySet<WebSocketController> webSocketSet) {
        WebSocketController.webSocketSet = webSocketSet;
    }

    public static CopyOnWriteArraySet<WebSocketController> getWebSocketSet() {
        return webSocketSet;
    }
}
