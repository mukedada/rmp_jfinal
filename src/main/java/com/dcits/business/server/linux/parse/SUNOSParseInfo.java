package com.dcits.business.server.linux.parse;

import com.dcits.business.server.linux.LinuxMonitoringInfo;

public class SUNOSParseInfo extends ParseInfo {
	
	protected SUNOSParseInfo() {
		
	}

	@Override
	public String parseMountDevice(String info, LinuxMonitoringInfo monitoringInfo) {
	    return "true";
	}

	@Override
	public String parseDeviceIOInfo(String info, LinuxMonitoringInfo monitoringInfo) {
		return "true";
	}
}
