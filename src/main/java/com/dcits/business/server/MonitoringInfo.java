package com.dcits.business.server;

import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 动态更新的监控信息
 * @author xuwangcheng
 * @version 1.0.0.0, 2018.3.28
 *
 */
public class MonitoringInfo {
	
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	protected Date time;
	
	
	public void setTime(Date time) {
		this.time = time;
	}
	
	public Date getTime() {
		return time;
	}
}
