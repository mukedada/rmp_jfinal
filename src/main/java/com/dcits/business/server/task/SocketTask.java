package com.dcits.business.server.task;

import com.dcits.business.server.ViewServerInfo;
import com.dcits.business.server.linux.LinuxServerController;
import com.dcits.business.server.websocker.WebSocketController;
import com.jfinal.plugin.cron4j.ITask;
import org.apache.log4j.Logger;

import javax.websocket.EncodeException;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * description: todo
 *
 * @author sun jun
 * @className SocketTask
 * @date 2020/9/7 14:26
 */
public class SocketTask implements ITask {
    /**
     *  userkey:viewServerInfo
     *
     */
    public static Map<String, List<ViewServerInfo>> serverMap = new HashMap<String, List<ViewServerInfo>>();

    public static final Logger logger = Logger.getLogger(SocketTask.class);

    @Override
    public void run() {
        try {
            WebSocketController.sendConnectInfo();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("轮询失败，" + e.getMessage());
        }

    }

    @Override
    public void stop() {
        System.out.println("定时结束轮询" + new Date());
    }

}
